const mongoose= require('mongoose')

let studentSchema = new mongoose.Schema({
    Name: {
         type: String,
         required:[true,'Name Field is required']
    },
    DOB: {
        type: Date,
        required:[true,'Select DOB'],
        default: Date.now
    },
    Gender: {
        type: String,
        required:[true,'Select Gender']
    },
    FatherName: {
        type: String,
        required:[true,'Fathername is required']
    },
    MotherName: {
        type: String,
        required:[true,'Mothername is required']
    },
    Address: {
        type: String,
        required:[true,'Address Field is required']
    },
    userId: {
        type: String,
        required:[true,'Address Field is required']
    }
    });
    
module.exports = mongoose.model('Student', studentSchema)