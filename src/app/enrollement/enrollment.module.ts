import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EnrollementComponent } from './enrollement.component';

/**
 * Login module used to maintain all the login details
 */
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [EnrollementComponent]
})
export class Enrollement {}
