import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnquiryComponent } from './enquiry.component';
function getHTMLInputElement(
  elem: HTMLElement,
  elQuery: string
): HTMLInputElement {
  return elem.querySelector(elQuery) as HTMLInputElement ;
}

describe('EnquiryComponent', () => {
  let component: EnquiryComponent;
  let fixture: ComponentFixture<EnquiryComponent>;
  let nativeElem: HTMLElement;
  let submitbtn: HTMLButtonElement;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnquiryComponent ]

    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnquiryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should contain textbox element as number', async () => {
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      number = getHTMLInputElement(nativeElem, 'input[name*="number"]');
      expect(number).toBeTruthy();
    });
  });
  it('should contain the submit button', async () => {
    submitbtn = (
    nativeElem.querySelector('button[type*="submit"]') as HTMLButtonElement
    );
    expect(submitbtn).toBeTruthy();
  });
  it('submit button should contain text "submit"', async () => {
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      submitbtn = (
        nativeElem.querySelector('button[type*="submit"]') as HTMLButtonElement
      );
      expect(submitbtn.textContent).toContain('submit');
    });
  });
  it('Enter the enquiry id', async () => {
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      component.loginUser({
        enq: 'anitha',
      });
      expect(component.loginUser).toBeFalsy();
    });
    component.ngOnInit();

  });

});
