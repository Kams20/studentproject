import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertService } from '../service/alert.service';
import { EnrollService } from '../service/enroll.service';
@Component({
  selector: 'app-enquiry',
  templateUrl: './enquiry.component.html',
  styleUrls: ['./enquiry.component.css']
})
export class EnquiryComponent implements OnInit {
enquiryForm: FormGroup;
submitted = false;

  constructor(private route: ActivatedRoute,
              private formBuilder: FormBuilder,
              private router: Router,
              private enrollService: EnrollService,
              private alertService: AlertService) {
                this.getForm();
               }
              getForm() {
                  this.enquiryForm = this.formBuilder.group({
                      id: ['', Validators.required]
                  });
               }
              getStudentt(id) {
              this.submitted = true;
              this.enrollService.getStudentt(id);
              // console.log('Result= %o',result);
              }
  ngOnInit() {
  }
  get f() { return this.enquiryForm.controls; }

}
