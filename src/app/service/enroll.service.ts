import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import axios, { AxiosRequestConfig, AxiosPromise } from 'axios';

@Injectable({
    providedIn: 'root'
  })


export class EnrollService {

    uri = 'http://localhost:4000/students';
  enquiryForm: any;

    constructor(private http: HttpClient) { }
    getData: any;

    addStudentt(Name, DOB, Gender, FatherName, MotherName, Address) {
          const obj = {
              // tslint:disable-next-line:object-literal-shorthand
              Name: Name,
              // tslint:disable-next-line:object-literal-shorthand
              DOB: DOB,
              // tslint:disable-next-line:object-literal-shorthand
              Gender: Gender,
              // tslint:disable-next-line:object-literal-shorthand
              FatherName: FatherName,
              // tslint:disable-next-line:object-literal-shorthand
              MotherName: MotherName,
              // tslint:disable-next-line:object-literal-shorthand
              Address: Address
          };
          console.log('Enrollement text: %j ' , obj);
          // this.http.post(this.uri + '/', obj);
          axios.post(this.uri + '/add',
          obj,
          {
            headers: {
                'Content-Type': 'application/json'
            }
         })
         .then((res) => {
             console.log('Response from server : %j', res);
             alert('SUCCESS!! :-)');
         }).then((data) => {
            console.log('Response from server : %j', data);
         });
        }
        getStudentt(id) {
          console.log('Enquiry text: %j', id);
          axios.get(this.uri + '/' + id)
          .then((res) => {
              console.log('Response from server : %j', res);
              this.getData = JSON.stringify(res.data);
              // alert(document.getElementById('resultTr').innerHTML);
              // alert('SUCCESS!! :-)\n\n' + this.getData);
             // document.getElementById('resultDiv').innerHTML = this.getData;
// tslint:disable-next-line: max-line-length
              document.getElementById('resultTr').innerHTML = '<td>' + res.data.Name + '</td><td>' + res.data.DOB + '</td><td>' + res.data.Gender + '</td><td>' + res.data.FatherName
              + '</td><td>' + res.data.MotherName + '</td><td>' + res.data.Address + '</td>';
          });
          }
}
