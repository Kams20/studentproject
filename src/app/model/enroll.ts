export default class Enroll {
    id: number;
    Name: string;
    DOB: Date;
    Gender: number;
    FatherName: string;
    MotherName: string;
    Address: string;
}
